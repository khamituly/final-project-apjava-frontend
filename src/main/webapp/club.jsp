<%@ page import="services.FetchService" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="models.Student" %>
<%@ page import="models.Club" %><%--
  Created by IntelliJ IDEA.
  User: Пользователь
  Date: 15.11.2020
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>

<link rel="stylesheet" href="https://bootstraptema.ru/plugins/2015/bootstrap3/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<script src="https://bootstraptema.ru/plugins/jquery/jquery-1.11.3.min.js"></script>
<script src="https://bootstraptema.ru/plugins/2015/b-v3-3-6/bootstrap.min.js"></script>


<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>club page</title>


    <style>
        body{background:url(https://bootstraptema.ru/images/bg/bg-1.png)}

        #main {
            background-color: #f2f2f2;
            padding: 20px;
            -webkit-border-radius: 4px;
            -moz-border-radius: 4px;
            -ms-border-radius: 4px;
            -o-border-radius: 4px;
            border-radius: 4px;
            border-bottom: 4px solid #ddd;
        }
        #real-estates-detail #author img {
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            -ms-border-radius: 100%;
            -o-border-radius: 100%;
            border-radius: 100%;
            border: 5px solid #ecf0f1;
            margin-bottom: 10px;
        }
        #real-estates-detail .sosmed-author i.fa {
            width: 30px;
            height: 30px;
            border: 2px solid #bdc3c7;
            color: #bdc3c7;
            padding-top: 6px;
            margin-top: 10px;
        }
        .panel-default .panel-heading {
            background-color: #fff;
        }
        #real-estates-detail .slides li img {
            height: 450px;
        }
    </style>
</head>
<body>
<%--you should to send userId and club id--%>

    <% FetchService fetchService = new FetchService();
        ArrayList<Student> students = new ArrayList<>();
        int clubId = Integer.parseInt(request.getParameter("cludId"));
        Club club = fetchService.fetchClub(clubId);
        students = fetchService.fetchStudentsOfClub(clubId);

        int studentId = Integer.parseInt(request.getParameter("studentId"));
        boolean existing = false;
        for(Student st: students){
            if(st.getId() == studentId ) existing = true;
        }
    %>


    <div class="container">
        <div id="main">


            <div class="row" id="real-estates-detail">
                <div class="col-lg-4 col-md-4 col-xs-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <header class="panel-title">
                                <div class="text-center">
                                </div>
                            </header>
                        </div>
                        <div class="panel-body">
                            <div class="text-center" id="author">
                                <img  style="width: 150px;" src="<%=club.getImage()%>>">
                                <h3><%=club.getName()%></h3>
                                <small class="label label-warning">University club</small>
                                <p><%=club.getDescription()%>></p>
                                <p class="sosmed-author">
                                    <a href="#"><i class="fa fa-facebook" title="Facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter" title="Twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram" title="Instagram"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" title="Linkedin"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-xs-12">
                    <div class="panel">
                        <div class="panel-body">
                            <ul id="myTab" class="nav nav-pills">
                                <li class="active"><a href="#detail" data-toggle="tab">Information</a></li>
                                <li class=""><a href="#contact" data-toggle="tab">Send a message</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content">
                                <hr>
                                <div class="tab-pane fade active in" id="detail">
                                    <h4>About club</h4>
                                    <table class="table table-th-block">
                                        <tbody>
                                        <tr><td class="active">Registration date:</td><td><%=club.getCreated_at()%>></td></tr>
                                        <tr><td class="active">University:</td><td>Astana IT University</td></tr>
                                        <tr><td class="active">Members:</td><td><%=students.size()%></td></tr>

                                        </tbody>

                                    </table>

                                    <%-- if the buttons are not displayed correctly it is most likely because of this because I did not fully understand how adding students to clubs works :)--%>

                                    <%if(!existing){%>
                                    <%--finish the code bt adding action for bellows form--%>
                                    <%-- to enter to this club--%>
                                    <form method="post" action="">
                                    <input type="submit" class="btn btn-success" data-original-title="" title="">enter</input>
                                    </form>
                                    <%}else{%>
                                    <%-- for leave from club if you already enter --%>
                                    <form method="post" action="">
                                    <input type="submit" class="btn btn-success" style="background-color: #ff5b67" data-original-title="" title="">leave</input>
                                    </form>

                                    <%}%>

                                </div>
                                <div class="tab-pane fade" id="contact">
                                    <p></p>
                                    <form role="form">
                                        <div class="form-group">
                                            <label>Your name</label>
                                            <input type="text" class="form-control rounded" placeholder="please enter a name">
                                        </div>
                                        <div class="form-group">
                                            <label>Your phone number</label>
                                            <input type="text" class="form-control rounded" placeholder="+7(775)123456">
                                        </div>
                                        <div class="form-group">
                                            <label>E-mail address</label>
                                            <input type="email" class="form-control rounded" placeholder="your e-mail ">
                                        </div>
                                        <div class="form-group">
                                            <label>Text of your message</label>
                                            <textarea class="form-control rounded" style="height: 100px;"></textarea>
                                            <p class="help-block">Your message will be a send to club</p>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-success" data-original-title="" title="">Send</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
</body>
</html>
